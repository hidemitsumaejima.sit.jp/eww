import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderSystem {
    private JPanel root;
    private JButton a3Button;
    private JButton a8Button;
    private JButton a1Button;
    private JButton a6Button;
    private JButton a7Button;
    private JButton a2Button;
    private JButton a4Button;
    private JButton a9Button;
    private JTextPane itemlist;
    private JLabel totalyen;
    private JButton cancel;
    private JButton checkout;

    int total = 0;
    int yen = 0;
    String GRADE;


    int price(String fruit) {
        if (fruit == "fruit_1") {
            return 110;
        } else if (fruit == "fruit_2") {
            return 220;
        } else if (fruit == "fruit_3") {
            return 330;
        } else if (fruit == "fruit_4") {
            return 440;
        } else if (fruit == "fruit_5") {
            return 550;
        } else if (fruit == "fruit_6") {
            return 660;
        } else if (fruit == "fruit_7") {
            return 770;
        } else if (fruit == "fruit_8") {
            return 880;
        }
        return 0;
    }

    void order(String fruit) {
        String FRUIT[] = {"S", "A", "NORMAL"};

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + fruit + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            int grade = JOptionPane.showOptionDialog(null,
                    "CHANGE GRADE OF FRUIT?",
                    "GRADE CHANGE",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    FRUIT,
                    FRUIT[0]);

            if (grade == 0) {
                grade += 50;
                GRADE = "S";
            } else if (grade == -1) {
                grade += 0;
                GRADE = "A";
            } else if (grade == 1) {
                grade -= 50;
                GRADE = "B";
            }
            /*else if (grade == 2) {
                grade -= 2;
                GRADE = "";
            }

             */

            yen = price(fruit) + grade;
            total += yen;
            totalyen.setText("Total     " + total + " yen");
            String currentText = itemlist.getText();
            itemlist.setText(currentText + GRADE + " " + fruit + " " + yen + "yen" + "\n");
            JOptionPane.showMessageDialog(null, "Order for " + GRADE + " " + fruit + " received.");

        }
    }

    public OrderSystem() {

        a1Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_1.png")));
        a2Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_2.png")));
        a3Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_3.png")));
        a4Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_4.png")));
        a6Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_5.png")));
        a7Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_6.png")));
        a8Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_7.png")));
        a9Button.setIcon(new ImageIcon(this.getClass().getResource("./img/fruit_8.png")));

        a1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_1");
            }
        });
        a2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_2");
            }
        });

        a3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_3");
            }
        });

        a4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_4");
            }
        });
        a6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_5");
            }
        });
        a7Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_6");
            }
        });

        a8Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_7");
            }
        });

        a9Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fruit_8");
            }
        });
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "CANCEL?",
                        "CANCEL",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    itemlist.setText(" ");
                    total = 0;
                    totalyen.setText("Total     " + total + " yen");
                }
            }
        });
        checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "CHECKOUT?",
                        "RECEIPT",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " yen.");
                    itemlist.setText(" ");
                    total = 0;
                    totalyen.setText("Total     " + total + " yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderSystem");
        frame.setContentPane(new OrderSystem().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setTitle("実験24 AF18093 前島英充");
    }
}